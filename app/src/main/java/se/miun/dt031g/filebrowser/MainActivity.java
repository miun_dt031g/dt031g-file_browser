package se.miun.dt031g.filebrowser;

import android.content.Intent;
import android.media.AudioManager;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.aditya.filebrowser.Constants;
import com.aditya.filebrowser.FileBrowser;
import com.aditya.filebrowser.FileChooser;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    private static final int PICK_FILE_REQUEST = 1;

    private Uri fileUri;
    private SoundPool soundPool;
    private int soundId = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        createSoundPool();
        displayExternalStoragePath();
    }

    private void displayExternalStoragePath() {
        File path = Environment.getExternalStorageDirectory();
        TextView pathTextView = findViewById(R.id.textView_external_path);
        pathTextView.setText(path.toString());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        destroySoundPool();
    }

    private void createSoundPool() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            soundPool = new SoundPool.Builder()
                    .setMaxStreams(1)
                    .build();
        } else {
            soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 1);
        }
    }

    private void destroySoundPool() {
        if (soundPool != null) {
            soundPool.release();
            soundPool = null;
        }
    }

    private void loadSound() {
        soundId = soundPool.load(fileUri.getPath(), 1);
    }

    // android:onClick="play" in activity_main.xml
    public void play(View view) {
        if (fileUri == null) {
            Toast.makeText(this, R.string.no_file_picked, Toast.LENGTH_SHORT).show();
        }
        else if (!fileUri.toString().endsWith("mp3") || soundId == -1) {
            Toast.makeText(this, R.string.can_not_play, Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(this, getResources().getString(R.string.playing_file) + soundId, Toast.LENGTH_SHORT).show();
            soundPool.play(soundId, 1, 1, 1, 0, 1);
        }
    }

    // android:onClick="pick" in activity_main.xml
    public void pick(View view) {
        // file picking (https://github.com/adityak368/Android-FileBrowser-FilePicker)
        Intent i = new Intent(getApplicationContext(), FileChooser.class);
        i.putExtra(Constants.SELECTION_MODE, Constants.SELECTION_MODES.SINGLE_SELECTION.ordinal());
        startActivityForResult(i, PICK_FILE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_FILE_REQUEST && data != null) {
            if (resultCode == RESULT_OK) {
                // Get picked file
                fileUri = data.getData();

                // Load sound
                loadSound();
            }
            else {
                fileUri = null;
            }

            // Update TextView
            String text = fileUri != null ? fileUri.getPath() : getString(R.string.pick_a_file);
            TextView v = (TextView) findViewById(R.id.textView_pick);
            v.setText(text);
        }
    }
}